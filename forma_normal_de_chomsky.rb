require './gramatica_libre_de_contexto'

class FormaNormalDeChomsky
  attr_reader :gramatica, :eliminables, :inicial, :producciones, :terminales, :base
  def initialize
    puts "Bienvenido!"
    puts "Desea ingresar una gramatica(1) o usar la de prueba(2):"
    puts "Elija una opcion 1 o 2:"
    eleccion = gets.chomp.to_i
    case eleccion
    when 1 then ingresar_valores
    when 2 then usar_default
    else puts "Ingreso un valor no valido"
    end
    @base = 82
  end
  
  protected
  def ejecucion
    paso_1
  end
  
  def paso_1
    paso(1)
    puts "Identificar variables eliminables"
    buscar_lambda
    puts "Variables con lambda: #{@eliminables}"
    ampliar_eliminables if @eliminables.length > 1
    separacion
    paso_2
  end

  def paso_2
    paso(2)
    if @eliminables.length == 0
      sustituir_terminales
      @producciones = @gramatica.producciones
      @inicial = @gramatica.inicial
      imprime_s
      imprime_p
      paso_4
    else
      puts "Ampliar"
      aumentar_inicial
      imprime_s
      aumentar_producciones
      puts ""
      imprime_p
      separacion
      paso_3
    end
  end

  def paso_3
    paso(3)
    elimina_lambda
    puts "Eliminar lambdas y sustituir los elementos unicos en S"
    @inicial[:S] = sustituir(@producciones, @inicial[:S].split(' | '))
    imprime_s
    puts ""
    imprime_p
    separacion
    paso_4
  end
  
  def paso_4
    paso(4)
    puts "Sustitucion de elementos terminales"
    ini = {}
    prod = {}
    sustituir_terminales
    @producciones.each do |key, value|
      prod[key.to_sym] = sustitucion_profunda(@terminales, value.split(' | '))
    end
    ini[:S] = sustitucion_profunda(@terminales, @inicial[:S].split(' | '))
    imprime_s ini
    puts ""
    imprime_p prod
    puts ""
    imprime_terminales
    separacion
    paso_5
  end

  def paso_5
    paso(5)
    puts "Forma Normal de Chosmky"
    imprime_p segmentar(@inicial)
    puts ""
    imprime_p segmentar(@producciones)
    puts ""
    imprime_terminales
    separacion
  end

  private
  def paso(n)
    puts "\n\t PASO #{n}"
    separacion
  end
  def separacion
    puts "============================================="
  end

  def ingresar_valores
    puts "Ingrese una Gramatica libre de contexto:"
    print "Σ (valores terminales):"
    sigma = gets.chomp
    print "V (valores no terminales):"
    v = gets.chomp
    print "S (elemento inicial):"
    s = gets.chomp
    print "P (producciones):"
    p = eval(gets.chomp)
    @gramatica = GramaticaLibreDeContexto.new(sigma, v, s, p)
    @gramatica.impresion
    ejecucion
  end
  def usar_default
    @gramatica = GramaticaLibreDeContexto.new
    @gramatica.impresion
    ejecucion
  end

  def buscar_lambda
    @eliminables = []
    @gramatica.producciones.each do |key, value|
      unless value.to_s.match('!') == nil
        @eliminables << key.to_s
      end
    end
  end

  def ampliar_eliminables
    estrella_eliminables = @eliminables
    estrella_eliminables << @eliminables.repeated_permutation(@eliminables.length).to_a
    @eliminables = estrella_eliminables.flatten
  end

  def aumentar_inicial
    @inicial = {}
    array = []
    array = @gramatica.inicial[:S].split(' | ')
    array.compact!
    array << @gramatica.no_terminales.join
    @inicial[:S] = (array << ampliar(array) << (@gramatica.no_terminales - @eliminables)).flatten.uniq.join(' | ')
  end

  def aumentar_producciones
    @producciones = {}
    array = []
    @gramatica.producciones.each do |key, value|
      array = value.split(' | ')
      @producciones[key.to_sym] = ampliar(array).join(' | ')
    end
  end

  def ampliar (array = [])
    nuevo_elemento = []
    array.each do |elemento|
      @eliminables.each do |eliminable|
        nuevo_elemento << elemento.delete(eliminable)
      end
    end
    nuevo_elemento.uniq!
  end

  def imprime_s(ini = @inicial)
    puts "S -> #{ini[:S]}"
  end

  def imprime_p(prod = @producciones)
    prod.each do |key, value|
      puts "#{key.to_s} -> #{value.to_s}"
    end
  end

  def imprime_terminales
    @terminales.each do |key, value|
      puts "#{value.to_s} -> #{key.to_s}"
    end
  end

  def elimina_lambda
    @producciones.each do |key, value|
      aux = value.split(" | ")
      @producciones[key.to_sym] = (aux - ['!']).join(' | ')
    end
  end

  def sustituir_terminales
    @terminales = {}
    @gramatica.terminales.each do |terminal|
      @terminales[terminal.to_sym] = ("X" + terminal)
    end
  end

  def sustituir (hash = {}, array = [])
    nuevo_array = []
    array.each do |elemento|
      unless hash[elemento.to_sym] == nil
        nuevo_array << hash[elemento.to_sym]
        array -= [elemento]
      end
    end
    ((array << nuevo_array).flatten).join(' | ')
  end

  def sustitucion_profunda (hash = {}, array = [])
    nuevo_array = []
    array.each do |elemento|
      aux = ''
      if elemento.length > 1
        elemento.each_char do |chr|
          if hash[chr.to_sym] == nil
            aux += chr
          else
            aux += (hash[chr.to_sym]).to_s
          end
        end
      else
        aux = elemento
      end
      nuevo_array << aux
    end
    (nuevo_array.flatten).join(' | ')
  end

  def segmentar(hash = {})
    diccionario = {}
    arreglo = []
    hash.each do |key, value|
      array = value.split(' | ')
      arreglo = []
      array.each do |elemento|
        if elemento.length > 2
          aux = {}
          for i in (0..(elemento.length-2))
            if i == 0
              aux[key.to_sym] = cambio_terminal(elemento[0].to_s) + @base.to_s + (i+1).to_s
            elsif i == elemento.length-2
              aux[(@base.to_s + (i).to_s).to_sym] = cambio_terminal(elemento[i].to_s) + cambio_terminal(elemento[i + 1].to_s)
            else
              aux[(@base.to_s + (i).to_s).to_sym] = cambio_terminal(elemento[i].to_s) + @base.to_s + (i+1).to_s
            end
          end
          @base = (@base.to_i + 1)
          imprime_p aux
          puts ""
        else
          ele = ''
          elemento.each_char do |chr|
            ele += cambio_terminal(chr).to_s
          end
          arreglo << ele
        end
      end
      arreglo.flatten!
      diccionario[key.to_sym] = arreglo.join(' | ')
    end
    diccionario
  end

  def cambio_terminal (chr = '')
    if @terminales[chr.to_sym] == nil
      chr
    else
      @terminales[chr.to_sym].to_s
    end
  end

end

chomsky = FormaNormalDeChomsky.new