class GramaticaLibreDeContexto
  #define setters para esta lista de variables de instancia
  attr_reader :terminales, :no_terminales, :inicial, :producciones
  # Metodo constructor, recibe S y P, donde S es un string
  # y P es una arreglo de Hashes, donde cada elemento es separado por ' | ' y lambda se define por '!'
  def initialize (sigma = 'a,b', v = 'A,C,D', s = 'AACD', p = {A: 'aAb | !', C: 'aC | a', D: 'aDa | bDb | !'})
    @inicial = {S: s}
    @producciones = p
    @terminales = sigma.split(',')
    @no_terminales = v.split(',')
  end
  def impresion
    puts "\tGramatica Libre de Contexto"
    puts "============================================="
    print "S -> #{@inicial[:S]}\n\n"
    @producciones.each do |key, value|
      print "#{key} -> #{value}\n"
    end
    puts "============================================="
  end
end
# ejemplo 1 de instaciación
#gramatica = GramaticaLibreDeContexto.new
#puts gramatica.terminales
#puts gramatica.no_terminales
#puts gramatica.inicial
#puts gramatica.producciones
#gramatica.impresion
# ejemplo 2 de instaciación
#gramatica = GramaticaLibreDeContexto.new('a,b,c','A,B,C','AB | AC | Aa',{A: 'Aa', B: 'BbA | aB', C: 'C'})
#puts gramatica.terminales
#puts gramatica.no_terminales
#puts gramatica.inicial
#puts gramatica.producciones